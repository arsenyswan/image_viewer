package com.example.arseniylebedev.imageviewer;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;




/**
 * Created by arseniy.lebedev on 18.07.2017.
 */

public interface WallpapersApi {
    @GET("get.php?")
    Call<СategoryHandler> getData(@Query("auth") String authKey, @Query("method") String nameOfMethod);
}

