package com.example.arseniylebedev.imageviewer;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class MainActivity extends AppCompatActivity {

    private static WallpapersApi wallpapersApi;
    RecyclerView recyclerView;
    List<CategoryModel> posts;
    List<CategoryModel> posts2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        wallpapersApi = Controller.getApi();

        posts = new ArrayList<>();

        recyclerView = (RecyclerView) findViewById(R.id.posts_recycle_view);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        PostsAdapter adapter = new PostsAdapter(posts);
        recyclerView.setAdapter(adapter);
 
        /* Пример вызова синхронного запроса. В главном потоке ТАБУ!
        try {
            Response response = wallpapersApi.getData("bash", 50).execute();
        } catch (IOException e) {
            e.printStackTrace();
        }*/

        wallpapersApi.getData("ba020371730439ba9abafad7f2d66c76", "category_list").enqueue(new Callback<СategoryHandler>() {
            @Override
            public void onResponse(Call<СategoryHandler> call, Response<СategoryHandler> response) {
                posts.addAll(response.body().getCategories());
                recyclerView.getAdapter().notifyDataSetChanged();
            }

            @Override
            public void onFailure(Call<СategoryHandler> call, Throwable t) {
                Toast.makeText(MainActivity.this, "An error occurred during networking", Toast.LENGTH_SHORT).show();
            }

           /* @Override
            public void onFailure(Call<List<CategoryModel>> call, Throwable t) {
                Toast.makeText(MainActivity.this, "An error occurred during networking", Toast.LENGTH_SHORT).show();
            }*/
        });
    }
}/*
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.android.volley.Cache;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {


    final String LOG_TAG = "myLogs";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final ListView lvMain = (ListView) findViewById(R.id.lvMain);

        String[] names = {"Марья", "Петр", "Антон", "Даша", "Борис",
                "Костя", "Игорь", "Анна", "Денис", "Андрей"};

        RequestQueue queue = Volley.newRequestQueue(this);
        String url = "https://wall.alphacoders.com/api2.0/get.php?auth=ba020371730439ba9abafad7f2d66c76&method=category_list";

// Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Display the first 500 characters of the response string.
                        Log.d(LOG_TAG, "Response is: " + response.substring(0, 500));
                        *//*Pattern p = Pattern.compile("\{X*\}");
                        Matcher m = p.matcher(response);
                        boolean b = m.matches();
                        *//*

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(LOG_TAG, "That didn't work!");
            }
        });

// Add the request to the RequestQueue.
        queue.add(stringRequest);

        Cache cache = queue.getCache();
        Cache.Entry entry = cache.get(url);
        if(entry != null){
            try {
                String data = new String(entry.data, "UTF-8");
                names = data.split(",");
                // handle data, like converting it to xml, json, bitmap etc.,
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }
    else{
        // Cached response doesn't exists. Make network call here
    }
        ArrayList<String> nNames = new ArrayList<String>();
        for (String str:names) {
            //nNames.add(str.substring(str.indexOf("i"),str.lastIndexOf("d")));
            if(str.length()>20) str.substring(20);
            nNames.add(str);

        }
        // создаем адаптер
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, nNames);

        // присваиваем адаптер списку
        lvMain.setAdapter(adapter);
        lvMain.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                Log.d(LOG_TAG, "itemClick: position = " + position + ", id = "
                        + id);
            }
        });


    }
}*/
