package com.example.arseniylebedev.imageviewer;

/**
 * Created by Arseniy.Lebedev on 18.07.2017.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class СategoryHandler {

    @SerializedName("success")
    @Expose
    private Boolean success;
    @SerializedName("categories")
    @Expose
    private List<CategoryModel> categories = null;

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public СategoryHandler withSuccess(Boolean success) {
        this.success = success;
        return this;
    }

    public List<CategoryModel> getCategories() {
        return categories;
    }

    public void setCategories(List<CategoryModel> categories) {
        this.categories = categories;
    }

    public СategoryHandler withCategories(List<CategoryModel> categories) {
        this.categories = categories;
        return this;
    }

}