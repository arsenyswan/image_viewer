package com.example.arseniylebedev.imageviewer;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by arseniy.lebedev on 18.07.2017.
 */

public class Controller {
    static final String BASE_URL = "https://wall.alphacoders.com/api2.0/";

    public static WallpapersApi getApi() {
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        WallpapersApi wallpapersApi = retrofit.create(WallpapersApi.class);
        return wallpapersApi;

    }
}