package com.example.arseniylebedev.imageviewer;

/**
 * Created by arseniy.lebedev on 18.07.2017.
 */

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;


public class PostsAdapter extends RecyclerView.Adapter<PostsAdapter.ViewHolder> {

    private List<CategoryModel> posts;

    public PostsAdapter(List<CategoryModel> posts) {
        this.posts = posts;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.post_item, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        CategoryModel post = posts.get(position);
       /* if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            holder.name.setText(Html.fromHtml(name.getUrl(), Html.FROM_HTML_MODE_LEGACY));
        } else {
            holder.name.setText(Html.fromHtml(name.getName()));
        }*/
        holder.name.setText(post.getName());
        holder.count.setText(post.getCount());
    }

    @Override
    public int getItemCount() {
        if (posts == null) return 0;
        return posts.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView name;
        TextView count;

        public ViewHolder(View itemView) {
            super(itemView);
            name = (TextView) itemView.findViewById(R.id.postitem_category);
            count = (TextView) itemView.findViewById(R.id.postitem_count);
        }
    }
}